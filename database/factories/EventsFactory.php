<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'name'              => $faker->words(3, true),
        'description'       => $faker->description,
        'event_date'        => now(),
        'maximum_attendies' => rand(20, 500),
        'speakers'          => rand(1, 5),
        'cancelled'         => false,
    ];
});
