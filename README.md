# [Talk] Static Analysis in PHP and Laravel

This repository is broken up into separate branches for the different steps of
the talk.

For the slides, please either visit the [pages] view, or go to the slides branch.

## Abstract

A discussion on how to use Static Analysis to prevent bugs in your Laravel Applications.
Bugs are very easy to produce by passing incorrect types around, and Laravel makes
this even more likely. So if you are looking to prevent bugs that might not be
caught by UnitTests, the best option is to implement Static Analysis.

## Presenter

Marisa Clardy

- [Website](https://clardy.eu/)
- Twitter: [@MarisaCodes](https://twitter.com/MarisaCodes)
- Github: [m50](https://github.com/m50)

[pages]: https://marisacodes.gitlab.io/talk-static-analysis-in-php-and-laravel/
