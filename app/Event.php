<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = ['id'];

    public static function booted()
    {
        static::saving(fn(Event $event) => $event->slug = Str::slug($event->name));
    }
}
