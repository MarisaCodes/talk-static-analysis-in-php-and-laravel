<?php

namespace App\Services;

/**
 * @template T of \App\Event
 */
class SomeService
{
    /**
     * @param T $eventThing
     * @return void
     */
    public function doSomething($eventThing): void
    {
        // this is where we do something...
        $eventThing->save();
    }
}
