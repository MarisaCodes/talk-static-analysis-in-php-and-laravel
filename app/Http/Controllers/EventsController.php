<?php

namespace App\Http\Controllers;

use App\Event;
use App\Services\SomeService;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Event::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = $request->validate([
            'name'              => 'string|min:5|max:100',
            'description'       => 'string',
            'event_date'        => 'date',
            'maximum_attendies' => 'int',
            'speakers'          => 'int',
        ]);

        $event = Event::create($valid);

        $someService = app(SomeService::class);

        $someService->doSomething($event);

        return response()->json($event);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return response()->json($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $valid = $request->validate([
            'name'              => 'string|required|min:5|max:100',
            'description'       => 'string',
            'event_date'        => 'required|date',
            'maximum_attendies' => 'required|int',
            'speakers'          => 'required|int',
        ]);

        $event->update($valid);

        return response()->json($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response('', 204);
    }

    /**
     * Handle the cancel route.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, Event $event)
    {
        $event->cancelled = true;
        $event->save();

        return response()->json($event);
    }
}
